package com.example.mbook;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.dao.DataIntegrityViolationException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookTest {

    @Autowired
    private BookRepository repo;

    private final static String title = "DevOps best practices";

    @Before
    public void deleteBookIfExist(){
        Book bookFound = repo.findByTitleContaining(title);
        if(bookFound != null)
            repo.deleteById(1l);
    }

    @Test
    @Rollback(false)
    public void testCreateBookWithSuccess(){
        Book book = new Book(1l, title,"Ngatchou","Cours 1");
        Book savedBook =  repo.save(book);
        assertNotNull(savedBook);
    }

    // @Test
    // public void testCreateNewBookFailedWithPropertyNull(){
    //     Book book = new Book(2l);
    //     DataIntegrityViolationException exception = assertThrows(DataIntegrityViolationException.class, () -> repo.save(book));
    //     assertNotNull(exception);
    // }

    // @Test
    // @Rollback(false)
    // public void testCreateNewBookFailedWithDuplicateEntry(){
    //     Book book = new Book(2l, title,"NDAYA","Cours 2");
    //     DataIntegrityViolationException exception = assertThrows(DataIntegrityViolationException.class, () -> repo.save(book));
    //     assertNotNull(exception);
    // }

    // @Test 
    // @Order(4)
    // public void testFindBookByTitleExist(){
    //     Book bookFound = repo.findByTitleContaining(title);
    //     assertEquals(bookFound.getTitle(), title);
    // } 

    // @Test
    // public void testFindBookByTitleNotExist(){
    //     String name = "Méthodes agile"; 
    //     Book bookFound = repo.findByTitleContaining(name);
    //     assertNull(bookFound);
    // } 

    // @Test
    // @Rollback
    // public void testUpdateBook(){

    //     String bookTitle = "Ing des tests";
    //     Book book = new Book(3l, bookTitle,"Ing Noubissie","description");

    //     Book savedBook = repo.save(book);
    //     Book bookToUpdate = savedBook;
    //     bookToUpdate.setAuthor("christian");
    //     savedBook = repo.save(bookToUpdate);

    //     assertNotNull(bookToUpdate);
    //     assertNotNull(savedBook);
    //     assertEquals(bookToUpdate.getAuthor(), savedBook.getAuthor());

    // }

    // @Test
    // @Rollback
    // public void testDeleteBook(){
    //     Long id = 1l;
    //     boolean isExistBeforeDelete = repo.findById(id).isPresent();
    //     repo.deleteById(id);
    //     boolean isExistAfterDelete = repo.findById(id).isPresent();

    //     assertTrue(isExistBeforeDelete);
    //     assertFalse(isExistAfterDelete);

    // }
}