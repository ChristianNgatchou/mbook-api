/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.mbook;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author merlintonka
 */
public interface BookRepository extends JpaRepository<Book, Long> {
    Book findByTitleContaining(String title);
}
