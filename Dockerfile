# # Docker Build Maven Stage
# FROM maven:3-jdk-8-alpine AS build
# # Copy folder in docker
# WORKDIR /opt/app
# COPY ./ /opt/app
# RUN mvn clean install -DskipTests
# # Run spring boot in Docker
# FROM openjdk:8-jdk-alpine
# COPY --from=build /opt/app/target/*.jar app.jar
# ENV PORT 9000
# EXPOSE $PORT
# ENTRYPOINT ["java","-jar","app.jar"]


FROM openjdk:11-jdk-slim

WORKDIR /app

COPY target/MBOOK_API-0.0.1-SNAPSHOT.jar /app/MBOOK_API-0.0.1-SNAPSHOT.jar

EXPOSE 9000

CMD ["java", "-jar", "MBOOK_API-0.0.1-SNAPSHOT.jar"]