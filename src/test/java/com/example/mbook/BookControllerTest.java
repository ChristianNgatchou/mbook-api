package com.example.mbook;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.example.mbook.BookController.NotFoundException;
import com.example.mbook.Book;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(BookController.class)
public class BookControllerTest {
    @Autowired
    MockMvc mockMvc;
    
    @Autowired
    ObjectMapper mapper;
    
    @MockBean
    BookRepository bookRepository;
    
    Book Book_1 = new Book(1l, "DevOps best practices","Rayven Yor","Cebu Philippines");
    Book Book_2 = new Book(2l, "Develop Angular Apps with Angular Material","David Landup","Cebu Philippines");
    Book Book_3 = new Book(3l, "Intro to Spring boot","Jane Doe","Cebu Philippines");
    
    // Get Test methods TBA
    @Test
    public void getAllBooks_success() throws Exception {
        List<Book> Books = new ArrayList<>(Arrays.asList(Book_1, Book_2, Book_3));
        
        Mockito.when(bookRepository.findAll()).thenReturn(Books);
        
        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/books")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[2].author", is("Jane Doe")));
    }

    @Test
    public void getBookById_success() throws Exception {
        Mockito.when(bookRepository.findById(Book_1.getId())).thenReturn(java.util.Optional.of(Book_1));

        mockMvc.perform(MockMvcRequestBuilders
                .get("/api/books/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.author", is("Rayven Yor")));
    }

    // @Test
    // public void getBookByTitle_success() throws Exception {
    //     List<Book> Books = new ArrayList<>(Arrays.asList(Book_1));

    //     // Mockito.when(bookRepository.findByTitleContaining("DevOps")).thenReturn(Books);

    //     mockMvc.perform(MockMvcRequestBuilders
    //             .get("/api/books/title/DevOps")
    //             .contentType(MediaType.APPLICATION_JSON))
    //             .andExpect(status().isOk())
    //             .andExpect(jsonPath("$", notNullValue()))
    //             .andExpect(jsonPath("$[0].author", is("Rayven Yor")));
    // }

    // Create Test methods TBA
    @Test
    public void createBook_success() throws Exception {
        Book Book = com.example.mbook.Book.builder()
                .title("C for beginners")
                .author("John Doe")
                .description("A complete guide for the basis on c")
                .build();

        Mockito.when(bookRepository.save(Book)).thenReturn(Book);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/api/books")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(Book));

        mockMvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.author", is("John Doe")));
        }

    // Update Test methods TBA
    @Test
    public void updateBook_success() throws Exception {
        Book updatedBook = Book.builder()
                .id(1l)
                .title("DevOps best practices")
                .author("Armand Zambo")
                .description("The best practices on")
                .build();

        Mockito.when(bookRepository.findById(Book_1.getId())).thenReturn(Optional.of(Book_1));
        Mockito.when(bookRepository.save(updatedBook)).thenReturn(updatedBook);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/books")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(updatedBook));

        mockMvc.perform(mockRequest)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.author", is("Armand Zambo")));
    }

    @Test
    public void updateBook_nullId() throws Exception {
        Book updatedBook = Book.builder()
                .title("Angular for beginners")
                .author("Sherlock Holmes")
                .description("221B Baker Street")
                .build();

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/books")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(updatedBook));

        mockMvc.perform(mockRequest)
                .andExpect(status().isBadRequest())
                .andExpect(result ->
                    assertTrue(result.getResolvedException() instanceof BookController.InvalidRequestException))
        .andExpect(result ->
            assertEquals("Book or ID must not be null!", result.getResolvedException().getMessage()));
    }

    /*@Test
    public void updateBook_BookNotFound() throws Exception {
        Book updatedBook = Book.builder()
                .id(5l)
                .title("R For beginners")
                .author("Sherlock Holmes")
                .description("221B Baker Street")
                .build();

        Mockito.when(bookRepository.findById(updatedBook.getId())).thenReturn(null);

        MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/api/books")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(this.mapper.writeValueAsString(updatedBook));

        mockMvc.perform(mockRequest)
                .andExpect(status().isBadRequest())
                .andExpect(result ->
                    assertTrue(result.getResolvedException() instanceof BookController.NotFoundException))
        .andExpect(result ->
            assertEquals("Book with ID 5 does not exist.", result.getResolvedException().getMessage()));
    }*/

    // Delete Test methods TBA
    @Test
    public void deleteBookById_success() throws Exception {
        Mockito.when(bookRepository.findById(Book_2.getId())).thenReturn(Optional.of(Book_2));

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/books/2")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    /*@Test
    public void deleteBookById_notFound() throws Exception {
        Mockito.when(bookRepository.findById(5l)).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/books/5")
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
                .andExpect(result ->
                        assertTrue(result.getResolvedException() instanceof NotFoundException))
        .andExpect(result ->
                assertEquals("Book with ID 5 does not exist.", result.getResolvedException().getMessage()));
    }*/
 
}
