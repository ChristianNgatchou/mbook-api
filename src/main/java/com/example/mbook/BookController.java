/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.mbook;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

/**
 *
 * @author merlintonka
 */
@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api/books")
public class BookController {
 
    @Autowired
    private BookRepository bookRepository;
        
    @GetMapping
    public List<Book> getAllBooks() {
        return (List<Book>) bookRepository.findAll();
    }
 
    @GetMapping("/{id}")
    public ResponseEntity<Book> getBookById(@PathVariable(value = "id") int id) {
       Optional<Book> book = bookRepository.findById(Long.valueOf(id));
 
        if(book.isPresent()) {
            return ResponseEntity.ok().body(book.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping("/title/{title}")
    public List<Book> getBookByTitle(@PathVariable(value = "title") String title) {
        return (List<Book>) bookRepository.findByTitleContaining(title);

    }

    @PostMapping
    public Book createBook(@Validated @RequestBody Book book) {
        return bookRepository.save(book);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    class InvalidRequestException extends RuntimeException {
        public InvalidRequestException(String s) {
            super(s);
        }
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    class NotFoundException extends RuntimeException {
        public NotFoundException(String s) {
            super(s);
        }
    }

    @PutMapping
    public Book updateBook(@RequestBody Book book) throws NotFoundException {
        if (book == null || book.getId() == null) {
            throw new InvalidRequestException("Book or ID must not be null!");
        }
        Optional<Book> optionalBook = bookRepository.findById(book.getId());
        if (!optionalBook.isPresent()) {
            throw new NotFoundException("Book with ID " + book.getId() + " does not exist.");
        }
        Book existingBook = optionalBook.get();

        existingBook.setTitle(book.getTitle());
        existingBook.setAuthor(book.getAuthor());
        existingBook.setDescription(book.getDescription());
        
        return bookRepository.save(existingBook);
    }
    
    @DeleteMapping("/{id}")
    public void deleteBookById(@PathVariable(value = "id") Long bookId) throws NotFoundException {
        if (!bookRepository.findById(bookId).isPresent()) {
            throw new NotFoundException("Book with ID " + bookId + " does not exist.");
        }
        bookRepository.deleteById(bookId);
    }

}
